import { environment } from './../../environments/environment';
import { Injectable } from '@angular/core';
import { HttpClient,  HttpHeaders } from '@angular/common/http';
import { RespuestaTopHeadlines } from '../interfaces/interfaces';

const apiKey = environment.apiKey;
const apiUrl = environment.apiUrl;

const headers = new HttpHeaders({
  'X-Api-Key': apiKey
});


@Injectable({
  providedIn: 'root'
})
export class NoticiasService {

  headlinesPages = 0;
  categoriaActual = '';
  categoriaPage = 0;
 // inportar el cliente hhtpclient
  constructor(private http: HttpClient) { }

  private ejecutarQuery<T>(query: string) {
    query = apiUrl + query;
    console.log('esta es' + query);
    return this.http.get<T>( query, {headers});
  }
  getTopHeadLines() {
    this.headlinesPages = this.headlinesPages + 1;
     // return this.ejecutarQuery<RespuestaTopHeadlines>(`/everything?q=bitcoin&from=2020-02-04&sortBy=publishedAt`);
    return this.ejecutarQuery<RespuestaTopHeadlines>(`/top-headlines?country=us&page=${ this.headlinesPages }`);
     // tslint:disable-next-line:max-line-length
    // return this.http.get<RespuestaTopHeadlines>(`http://newsapi.org/v2/everything?q=bitcoin&from=2020-01-28&sortBy=publishedAt&apiKey=f91d78b45235429db8e96e2e7917a4d6`);
  }
  getTopHeadLinesCategoria( categoria: string) {
    if (this.categoriaActual === categoria ) {
      this.categoriaPage++;
    } else {
      this.categoriaPage = 1;
      this.categoriaActual = categoria;
    }
     // return this.http.get(`http://newsapi.org/v2/top-headlines?country=us&category=business&apiKey=f91d78b45235429db8e96e2e7917a4d6`);
    return this.ejecutarQuery<RespuestaTopHeadlines>(`/top-headlines?country=us&category=${categoria}&pages=${this.categoriaPage}`);
    }
}
