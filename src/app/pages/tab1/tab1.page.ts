import { NoticiasService } from './../../services/noticias.service';
import { Component, OnInit } from '@angular/core';
import { RespuestaTopHeadlines, Article } from '../../interfaces/interfaces';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page implements OnInit {

  noticias: Article[] = [];

  constructor(private noticiasService: NoticiasService) {

  }

  ngOnInit() {
    // tslint:disable-next-line: no-debugger
    // this.noticiasService.getTopHeadLines()
    // .subscribe( resp => {
    //   console.log('noticias', resp);
    //   // this.noticias = resp.articles;
    //   // Añado todos los articulos al arreglo
    //   // voy a utilizar el operador spread para que me separe los articulos en el resp con ...
    //   this.noticias.push( ...resp.articles );
    // });
    this.caragarNoticias(  );
  }
  loadData( event ) {
    console.log(event);
    this.caragarNoticias( event );
  }

  caragarNoticias( event? ) {
    this.noticiasService.getTopHeadLines()
    .subscribe( resp => {
      console.log('noticias', resp);
      if ( resp.articles.length === 0) {
        event.target.desables = true;
        event.target.complete();
      }
      // this.noticias = resp.articles;
      // Añado todos los articulos al arreglo
      // voy a utilizar el operador spread para que me separe los articulos en el resp con ...
      this.noticias.push( ...resp.articles );

      if ( event ) {
        event.target.complete();
      }
    });
  }

}
